class Trash < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :purchasable, :currency_type, :currency_value,
    :description, :icon_name, :name, :tags, :size, :size_type

  validates :code, :purchasable, :currency_type, :currency_value,
    :description, :icon_name, :name, :tags, :size, :size_type,
    :presence => true

  validates :size_type, :inclusion => ["medium", "big"]
end

