class Element < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :purchasable, :description, :icon_name, :name, :tags

  validates :code, :purchasable, :description,
      :icon_name, :name, :tags,
      :presence => true
end
