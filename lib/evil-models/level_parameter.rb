class LevelParameter < ActiveRecord::Base
  include Hashable

  attr_accessible :level, :xp

  validates :level, :xp, :presence => true

end
