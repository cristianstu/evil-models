class MachineFood < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :purchasable, :description, :icon_name, :name, :tags,
    :currency_type, :currency_value, :size, :food_max_amount, :food_id,
    :food_creation_time, :wins_needed, :max_amount

  belongs_to :food

  validates :code, :purchasable, :description, :icon_name, :name, :tags,
    :currency_type, :currency_value, :size, :food_max_amount, :food_id,
    :food_creation_time, :wins_needed, :max_amount,
    :presence => true

end

