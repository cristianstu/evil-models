class Decoration < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :name, :purchasable, :currency_type, :currency_value, :description,
    :icon_name, :tags,	:size, :time_moral_decrement

  validates :code, :name, :purchasable, :currency_type, :currency_value, :description,
    :icon_name, :tags,	:size, :time_moral_decrement,
    :presence => true
 
end
