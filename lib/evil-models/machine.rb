class Machine < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :name, :purchasable, :currency_type, :currency_value, :description,
    :icon_name, :tags,	:size, :level_monster_required, :strength, :speed, :dexterity,
    :stamina_per_point, :training_type, :max_amount

  validates :code, :name, :purchasable, :currency_type, :currency_value, :description,
    :icon_name, :tags,	:size, :level_monster_required, :strength, :speed, :dexterity, 
    :stamina_per_point, :training_type, :max_amount,
    :presence => true
end

