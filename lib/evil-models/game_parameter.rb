class GameParameter < ActiveRecord::Base
  include Hashable

  attr_accessible :code, :start_hard_coin, :start_soft_coin,
    :start_ticket_coin, :max_time_auto_train,
    :roulette_cooldown

  validates :code, :start_hard_coin, :start_soft_coin,
    :start_ticket_coin, :max_time_auto_train,
    :roulette_cooldown, :presence => true
end

