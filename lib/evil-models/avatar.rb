class Avatar < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :name, :purchasable, :currency_type, :currency_value,
    :time_arrival, :description, :description_arrival, :icon_name, :tags,
    :strength_start, :speed_start, :dexterity_start, :cap_skills, :element_id,
    :amount_element, :deco_arrival_id


  belongs_to :element
  belongs_to :deco_arrival

  validates :code, :name, :purchasable, :currency_type, :currency_value,
      :time_arrival, :description, :description_arrival, :icon_name,
      :tags, :strength_start, :speed_start, :dexterity_start, :cap_skills,
      :element_id, :amount_element, :deco_arrival_id,
      :presence => true
end
