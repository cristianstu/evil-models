module Hashable

  def self.included(base)
    base.extend(ClassMethods)
  end

  def to_hash
    self.as_json(:except => :id)
  end

  module ClassMethods
    def all_to_hash
      self.all.as_json(:except => :id)
    end
  end

end
