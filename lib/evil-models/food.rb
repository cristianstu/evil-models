class Food < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :purchasable, :description, :currency_type,
    :currency_value, :icon_name, :name, :tags, :stamina

  validates :code, :purchasable, :description, :currency_type,
    :currency_value, :icon_name, :name, :tags, :stamina,
    :presence => true

end

