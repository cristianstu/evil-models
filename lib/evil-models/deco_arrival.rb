class DecoArrival < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :purchasable, :description, :icon_name, :name, :tags, :size

  validates :code, :purchasable, :description,
      :icon_name, :name, :tags, :size,
      :presence => true
end
