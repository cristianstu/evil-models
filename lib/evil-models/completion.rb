class Completion < ActiveRecord::Base

  include Hashable

  attr_accessible :code, :purchasable, :description, :currency_type,
    :currency_value, :icon_name, :name, :tags, :monsters_need,
    :tickets_need, :currency_reward, :xp_reward, :stamina_required,
    :items_reward

  validates :code, :purchasable, :description, :currency_type,
    :currency_value, :icon_name, :name, :tags, :monsters_need,
    :tickets_need, :currency_reward, :xp_reward, :stamina_required, 
    :items_reward,
    :presence => true

end

