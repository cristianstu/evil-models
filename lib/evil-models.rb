require "evil-models/hashable"
require "evil-models/avatar"
require "evil-models/completion"
require "evil-models/decoration"
require "evil-models/deco_arrival"
require "evil-models/element"
require "evil-models/machine"
require "evil-models/machine_food"
require "evil-models/food"
require "evil-models/game_parameter"
require "evil-models/level_parameter"
require "evil-models/trash"
require "evil-models/version"

module Evil
  module Models
    # Your code goes here...
  end
end
