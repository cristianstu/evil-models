# -*- encoding: utf-8 -*-
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'evil-models/version'

Gem::Specification.new do |gem|
  gem.name          = "evil-models"
  gem.version       = Evil::Models::VERSION
  gem.authors       = ["Cristian Stügelmayer"]
  gem.email         = ["cristians@redkatana.com"]
  gem.description   = "Evil Olympics Models"
  gem.summary       = "Evil Olympics Models"
  gem.homepage      = ""

  gem.files         = `git ls-files`.split($/)
  gem.executables   = gem.files.grep(%r{^bin/}).map{ |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ["lib"]
end
